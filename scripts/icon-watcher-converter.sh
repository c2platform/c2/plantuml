#!/bin/bash

# Base directory where the icons directory is located
BASE_DIR="$(pwd)"

# Directories to watch
PNG_DIR="$BASE_DIR/icons/png"
SVG_DIR="$BASE_DIR/icons/svg"
PUML_DIR="$BASE_DIR/icons/puml"

# Output file for PUML sprites
OUTPUT_FILE="$BASE_DIR/sprites.puml"

# Python scripts for conversions
CONVERT_PNG_SCRIPT="$BASE_DIR/scripts/png2puml.py"
CONVERT_SVG_SCRIPT="$BASE_DIR/scripts/svg2png.py"
UPDATE_README_SCRIPT="$BASE_DIR/scripts/readme.py"

# Check if inotifywait is available
if ! command -v inotifywait &> /dev/null; then
    echo "inotifywait could not be found. Please install inotify-tools."
    exit 1
fi

# Function to concatenate all .puml files into sprites.puml
concatenate_puml() {
    cat "$PUML_DIR"/*.puml > "$OUTPUT_FILE"
    echo "Updated sprites.puml"
}

# Function to convert PNG to PlantUML sprite
convert_png() {
    local file="$1"
    python "$CONVERT_PNG_SCRIPT" "$file"
}

# Function to convert SVG to PNG
convert_svg() {
    local file="$1"
    python3 "$CONVERT_SVG_SCRIPT" "$file"
}

# Function to update readme
update_readme() {
    python3 "$UPDATE_README_SCRIPT"
    echo "Updated README.md"
}


# Initial concatenation of .puml files
concatenate_puml

# Main loop to watch the specified directories for file changes
inotifywait -m -e close_write,moved_to,create -r "$PNG_DIR" "$SVG_DIR" "$PUML_DIR" --format "%w%f %e" |
while read file event; do
    # Determine action based on file extension and directory
    if [[ $file == *.png ]] || [[ $file == *.PNG ]]; then
        convert_png "$file"
    elif [[ $file == *.svg ]] || [[ $file == *.SVG ]]; then
        convert_svg "$file"
    elif [[ $file == *.puml ]] || [[ $file == *.PUML ]]; then
        concatenate_puml
        update_readme
    fi
done
