import sys
import subprocess
from pathlib import Path
from lxml import etree
import re
import os


# Function to parse dimension and convert to pixels if necessary
def parse_dimension(value):
    DPI = 96  # Default DPI for converting units to pixels
    unit_to_inch = {"mm": 1 / 25.4, "cm": 1 / 2.54, "in": 1, "px": 1 / DPI}

    match = re.match(r"([0-9.]+)(mm|cm|in|px)?", value)
    if match:
        num, unit = match.groups()
        num = float(num)
        unit = unit if unit else "px"  # Default to pixels if no unit is specified
        return int(num / unit_to_inch[unit] * DPI)
    else:
        raise ValueError(f"Invalid dimension value: {value}")


def get_svg_size(svg_path):
    tree = etree.parse(svg_path)
    svg_root = tree.getroot()
    width = parse_dimension(svg_root.get("width"))
    height = parse_dimension(svg_root.get("height"))
    return width, height


def validate_file_name(file_name):
    # Regular expression to match valid PlantUML sprite names
    if not re.match(r"^[a-zA-Z_]\w*$", file_name):
        raise ValueError(f"Invalid file name for PlantUML sprite: '{file_name}'")


def export_png(svg_path, width, height, suffix):
    output_path = svg_path.parent.parent / "png" / f"{svg_path.stem}{suffix}.png"
    output_path.parent.mkdir(parents=True, exist_ok=True)
    inkscape_command = [
        "inkscape",
        str(svg_path),  # Ensure the path is stringified for the command
        "--export-filename",
        str(output_path),  # Ensure the path is stringified for the command
        "--export-width",
        str(width),
        "--export-height",
        str(height),
        "--export-background-opacity=1",
    ]
    subprocess.run(inkscape_command, check=True)
    print(f"svg2png: {os.path.basename(svg_path)} → {os.path.basename(output_path)}")


def main(svg_file):
    svg_path = Path(svg_file)
    if not svg_path.exists() or svg_path.suffix.lower() != ".svg":
        print("Invalid SVG file.")
        return

    try:
        # Validate file name before proceeding
        validate_file_name(svg_path.stem)

        width, height = get_svg_size(svg_path)

        # Check for less than 5% difference
        dimension_difference = abs(width - height) / max(width, height)
        if dimension_difference < 0.09:  # If difference is less than 5%
            # Resize with aspect ratio intact, ensuring max dimension is 50 or less
            max_dimension = max(width, height)
            scale_factor = min(
                50 / max_dimension, 1
            )  # Ensure max is 50 or original size if smaller
            new_width = int(width * scale_factor)
            new_height = int(height * scale_factor)
            export_png(svg_path, new_width, new_height, "")
        else:  # If difference is 9% or more
            max_size = 50
            if width > height:
                export_png(
                    svg_path, max_size, int((max_size / width) * height), "_max50"
                )
                export_png(
                    svg_path, int((max_size / height) * width), max_size, "_min50"
                )
            else:
                export_png(
                    svg_path, int((max_size / height) * width), max_size, "_max50"
                )
                export_png(
                    svg_path, max_size, int((max_size / width) * height), "_min50"
                )
    except ValueError as e:
        print(e)
        return  # Skip to the next file if the current one has an invalid name


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <path_to_svg>")
    else:
        main(sys.argv[1])
