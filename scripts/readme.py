import os
from jinja2 import Template
from pathlib import Path

directory = "./icons/puml"
template_str = """

{% for file in files %}
Boundary({{ file }}, "{{ file }}") {
    System({{ file }}_system, "{{ file }}", "{{ file }}", $sprite="{{ file }}")
    System_Ext({{ file }}_system_ext, "{{ file }}", "{{ file }}", $sprite="{{ file }}")
    Container({{ file }}_container, "{{ file }}", "{{ file }}", $sprite="{{ file }}")
    Container_Ext({{ file }}_container_ext, "{{ file }}", "{{ file }}", $sprite="{{ file }}")
    Component({{ file }}_component, "{{ file }}", "{{ file }}", $sprite="{{ file }}")
    Component_Ext({{ file }}_component_ext, "{{ file }}", "{{ file }}", $sprite="{{ file }}")
    Lay_Right({{ file }}_system, {{ file }}_container)
    Lay_Right({{ file }}_container, {{ file }}_component)
    Lay_Right({{ file }}_component, {{ file }}_system_ext)
    Lay_Right({{ file }}_system_ext, {{ file }}_container_ext)
    Lay_Right({{ file }}_container_ext, {{ file }}_component_ext)
    {%- if not loop.first %}
    Lay_Up({{ file }}_system, {{ loop.previtem }}_system)
    {%- endif %}
}
{% endfor %}
"""


# Function to find files and generate markdown
def generate_markdown(directory, template_str):
    files = [
        Path(f).stem
        for f in os.listdir(directory)
        if os.path.isfile(os.path.join(directory, f))
    ]
    template = Template(template_str)
    markdown_output = template.render(files=files)
    return markdown_output


# Generate the markdown
markdown = generate_markdown(directory, template_str)


# File handling to update markdown content between specific tags
def update_readme(file_path, start_tag, end_tag, new_content):
    with open(file_path, "r") as file:
        lines = file.readlines()

    start_idx = None
    end_idx = None

    for i, line in enumerate(lines):
        if start_tag in line:
            start_idx = i
        elif end_tag in line:
            end_idx = i
            break

    if start_idx is not None and end_idx is not None:
        new_lines = lines[: start_idx + 1] + [new_content + "\n"] + lines[end_idx:]
        with open(file_path, "w") as file:
            file.writelines(new_lines)


# Path to your README.md file
readme_path = "README.md"

# Update the README.md file
update_readme(
    readme_path, "start-generated-plantuml", "end-generated-plantuml", markdown
)
