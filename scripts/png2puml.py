import os
import sys
import requests
from pathlib import Path
import subprocess

plantuml_jar_url = "https://github.com/plantuml/plantuml/releases/download/v1.2024.0/plantuml-1.2024.0.jar"
plantuml_jar_path = "/tmp/plantuml-1.2024.0.jar"  # PlantUML jar file will be saved here


def download_plantuml_jar():
    if not Path(plantuml_jar_path).exists():
        print(
            f"Downloading PlantUML jar from {plantuml_jar_url} to {plantuml_jar_path}..."
        )
        response = requests.get(plantuml_jar_url)
        with open(plantuml_jar_path, "wb") as file:
            file.write(response.content)
        print("Download complete.")


def convert_png_to_sprite(png_file):
    puml_dir = Path(png_file).parent.parent / "puml"
    puml_dir.mkdir(parents=True, exist_ok=True)
    output_file = puml_dir / f"{Path(png_file).stem}.puml"

    command = ["java", "-jar", plantuml_jar_path, "-encodesprite", "8z", png_file]
    result = subprocess.run(command, capture_output=True, text=True, check=True)
    sprite_output = result.stdout

    with open(output_file, "w") as file:
        file.write(sprite_output)
    print(f"png2puml: {os.path.basename(png_file)} → {os.path.basename(output_file)}")


def main(png_file):
    download_plantuml_jar()
    convert_png_to_sprite(png_file)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python png2sprite.py <path_to_png>")
    else:
        main(sys.argv[1])
