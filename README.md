# C2 Platform PlantUML Repository

This repository contains PlantUML (`puml`) files used for the
[c2platform.org](https://c2platform.org) website.

## Usage

### Using C4-PlantUML

If you want to use
[C4-PlantUML](https://github.com/plantuml-stdlib/C4-PlantUML), include
[`c4.puml`](./c4.puml) in your PlantUML files by using the following line:

```
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
```

### Using Sprites Only

If you only want to use the sprites, include [`sprites.puml`](./sprites.puml)
with the following line:

```
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/sprites.puml?ref_type=heads
```

## Development

This repository includes scripts for generating PlantUML (`puml`) files based on
`svg` files.

### Scripts

- [`icon-watcher-converter.sh`](./scripts/icon-watcher-converter.sh): Uses
  `inotifywait` to detect changes and automatically convert `svg` to `png`,
  `png` to `puml`, and then update the [`sprites.puml`](./sprites.puml) file.
- [`svg2png.py`](./scripts/svg2png.py): Converts `svg` files to `png`.
- [`png2puml.py`](./scripts/png2puml.py): Converts `png` files to `puml`.

### Running the Converter

To start the icon watcher converter:

```bash
./scripts/icon-watcher-converter.sh
```

After starting `icon-watcher-converter.sh`, you can drop `svg` files into the
`icons/svg` directory, and the files will be generated and updated
automatically.

### Naming Conventions

- `svg` files should use underscores (`_`) instead of hyphens (`-`) in their
  names.
- For ease of use, it is recommended to use lowercase letters and separate words
  with underscores (`_`).

## Preview

This section provides a preview of the available PlantUML sprites in this
repository.

### Using Sprites

You can use the sprites with the `$sprite` attribute as shown below:

```
System(ide_min50_system, "ide_min50", "ide_min50", $sprite="ide_min50")
```

### Using Tags

You can also use tags:

```
AddSystemTag("GIS_System", $sprite="arcgis", $legendText="GIS platform")
```


```plantuml
@startuml sprites-preview
!include https://gitlab.com/c2platform/c2/plantuml/-/raw/master/c4.puml?ref_type=heads
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml
'!include sprites.puml
'start-generated-plantuml



Boundary(rs_ds, "rs_ds") {
    System(rs_ds_system, "rs_ds", "rs_ds", $sprite="rs_ds")
    System_Ext(rs_ds_system_ext, "rs_ds", "rs_ds", $sprite="rs_ds")
    Container(rs_ds_container, "rs_ds", "rs_ds", $sprite="rs_ds")
    Container_Ext(rs_ds_container_ext, "rs_ds", "rs_ds", $sprite="rs_ds")
    Component(rs_ds_component, "rs_ds", "rs_ds", $sprite="rs_ds")
    Component_Ext(rs_ds_component_ext, "rs_ds", "rs_ds", $sprite="rs_ds")
    Lay_Right(rs_ds_system, rs_ds_container)
    Lay_Right(rs_ds_container, rs_ds_component)
    Lay_Right(rs_ds_component, rs_ds_system_ext)
    Lay_Right(rs_ds_system_ext, rs_ds_container_ext)
    Lay_Right(rs_ds_container_ext, rs_ds_component_ext)
}

Boundary(rs_deploy, "rs_deploy") {
    System(rs_deploy_system, "rs_deploy", "rs_deploy", $sprite="rs_deploy")
    System_Ext(rs_deploy_system_ext, "rs_deploy", "rs_deploy", $sprite="rs_deploy")
    Container(rs_deploy_container, "rs_deploy", "rs_deploy", $sprite="rs_deploy")
    Container_Ext(rs_deploy_container_ext, "rs_deploy", "rs_deploy", $sprite="rs_deploy")
    Component(rs_deploy_component, "rs_deploy", "rs_deploy", $sprite="rs_deploy")
    Component_Ext(rs_deploy_component_ext, "rs_deploy", "rs_deploy", $sprite="rs_deploy")
    Lay_Right(rs_deploy_system, rs_deploy_container)
    Lay_Right(rs_deploy_container, rs_deploy_component)
    Lay_Right(rs_deploy_component, rs_deploy_system_ext)
    Lay_Right(rs_deploy_system_ext, rs_deploy_container_ext)
    Lay_Right(rs_deploy_container_ext, rs_deploy_component_ext)
    Lay_Up(rs_deploy_system, rs_ds_system)
}

Boundary(rs_c_role, "rs_c_role") {
    System(rs_c_role_system, "rs_c_role", "rs_c_role", $sprite="rs_c_role")
    System_Ext(rs_c_role_system_ext, "rs_c_role", "rs_c_role", $sprite="rs_c_role")
    Container(rs_c_role_container, "rs_c_role", "rs_c_role", $sprite="rs_c_role")
    Container_Ext(rs_c_role_container_ext, "rs_c_role", "rs_c_role", $sprite="rs_c_role")
    Component(rs_c_role_component, "rs_c_role", "rs_c_role", $sprite="rs_c_role")
    Component_Ext(rs_c_role_component_ext, "rs_c_role", "rs_c_role", $sprite="rs_c_role")
    Lay_Right(rs_c_role_system, rs_c_role_container)
    Lay_Right(rs_c_role_container, rs_c_role_component)
    Lay_Right(rs_c_role_component, rs_c_role_system_ext)
    Lay_Right(rs_c_role_system_ext, rs_c_role_container_ext)
    Lay_Right(rs_c_role_container_ext, rs_c_role_component_ext)
    Lay_Up(rs_c_role_system, rs_deploy_system)
}

Boundary(rs_ing, "rs_ing") {
    System(rs_ing_system, "rs_ing", "rs_ing", $sprite="rs_ing")
    System_Ext(rs_ing_system_ext, "rs_ing", "rs_ing", $sprite="rs_ing")
    Container(rs_ing_container, "rs_ing", "rs_ing", $sprite="rs_ing")
    Container_Ext(rs_ing_container_ext, "rs_ing", "rs_ing", $sprite="rs_ing")
    Component(rs_ing_component, "rs_ing", "rs_ing", $sprite="rs_ing")
    Component_Ext(rs_ing_component_ext, "rs_ing", "rs_ing", $sprite="rs_ing")
    Lay_Right(rs_ing_system, rs_ing_container)
    Lay_Right(rs_ing_container, rs_ing_component)
    Lay_Right(rs_ing_component, rs_ing_system_ext)
    Lay_Right(rs_ing_system_ext, rs_ing_container_ext)
    Lay_Right(rs_ing_container_ext, rs_ing_component_ext)
    Lay_Up(rs_ing_system, rs_c_role_system)
}

Boundary(ide_min50, "ide_min50") {
    System(ide_min50_system, "ide_min50", "ide_min50", $sprite="ide_min50")
    System_Ext(ide_min50_system_ext, "ide_min50", "ide_min50", $sprite="ide_min50")
    Container(ide_min50_container, "ide_min50", "ide_min50", $sprite="ide_min50")
    Container_Ext(ide_min50_container_ext, "ide_min50", "ide_min50", $sprite="ide_min50")
    Component(ide_min50_component, "ide_min50", "ide_min50", $sprite="ide_min50")
    Component_Ext(ide_min50_component_ext, "ide_min50", "ide_min50", $sprite="ide_min50")
    Lay_Right(ide_min50_system, ide_min50_container)
    Lay_Right(ide_min50_container, ide_min50_component)
    Lay_Right(ide_min50_component, ide_min50_system_ext)
    Lay_Right(ide_min50_system_ext, ide_min50_container_ext)
    Lay_Right(ide_min50_container_ext, ide_min50_component_ext)
    Lay_Up(ide_min50_system, rs_ing_system)
}

Boundary(rs_rb, "rs_rb") {
    System(rs_rb_system, "rs_rb", "rs_rb", $sprite="rs_rb")
    System_Ext(rs_rb_system_ext, "rs_rb", "rs_rb", $sprite="rs_rb")
    Container(rs_rb_container, "rs_rb", "rs_rb", $sprite="rs_rb")
    Container_Ext(rs_rb_container_ext, "rs_rb", "rs_rb", $sprite="rs_rb")
    Component(rs_rb_component, "rs_rb", "rs_rb", $sprite="rs_rb")
    Component_Ext(rs_rb_component_ext, "rs_rb", "rs_rb", $sprite="rs_rb")
    Lay_Right(rs_rb_system, rs_rb_container)
    Lay_Right(rs_rb_container, rs_rb_component)
    Lay_Right(rs_rb_component, rs_rb_system_ext)
    Lay_Right(rs_rb_system_ext, rs_rb_container_ext)
    Lay_Right(rs_rb_container_ext, rs_rb_component_ext)
    Lay_Up(rs_rb_system, ide_min50_system)
}

Boundary(browser_max50, "browser_max50") {
    System(browser_max50_system, "browser_max50", "browser_max50", $sprite="browser_max50")
    System_Ext(browser_max50_system_ext, "browser_max50", "browser_max50", $sprite="browser_max50")
    Container(browser_max50_container, "browser_max50", "browser_max50", $sprite="browser_max50")
    Container_Ext(browser_max50_container_ext, "browser_max50", "browser_max50", $sprite="browser_max50")
    Component(browser_max50_component, "browser_max50", "browser_max50", $sprite="browser_max50")
    Component_Ext(browser_max50_component_ext, "browser_max50", "browser_max50", $sprite="browser_max50")
    Lay_Right(browser_max50_system, browser_max50_container)
    Lay_Right(browser_max50_container, browser_max50_component)
    Lay_Right(browser_max50_component, browser_max50_system_ext)
    Lay_Right(browser_max50_system_ext, browser_max50_container_ext)
    Lay_Right(browser_max50_container_ext, browser_max50_component_ext)
    Lay_Up(browser_max50_system, rs_rb_system)
}

Boundary(jenkins_max50, "jenkins_max50") {
    System(jenkins_max50_system, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    System_Ext(jenkins_max50_system_ext, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Container(jenkins_max50_container, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Container_Ext(jenkins_max50_container_ext, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Component(jenkins_max50_component, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Component_Ext(jenkins_max50_component_ext, "jenkins_max50", "jenkins_max50", $sprite="jenkins_max50")
    Lay_Right(jenkins_max50_system, jenkins_max50_container)
    Lay_Right(jenkins_max50_container, jenkins_max50_component)
    Lay_Right(jenkins_max50_component, jenkins_max50_system_ext)
    Lay_Right(jenkins_max50_system_ext, jenkins_max50_container_ext)
    Lay_Right(jenkins_max50_container_ext, jenkins_max50_component_ext)
    Lay_Up(jenkins_max50_system, browser_max50_system)
}

Boundary(rs_pvc, "rs_pvc") {
    System(rs_pvc_system, "rs_pvc", "rs_pvc", $sprite="rs_pvc")
    System_Ext(rs_pvc_system_ext, "rs_pvc", "rs_pvc", $sprite="rs_pvc")
    Container(rs_pvc_container, "rs_pvc", "rs_pvc", $sprite="rs_pvc")
    Container_Ext(rs_pvc_container_ext, "rs_pvc", "rs_pvc", $sprite="rs_pvc")
    Component(rs_pvc_component, "rs_pvc", "rs_pvc", $sprite="rs_pvc")
    Component_Ext(rs_pvc_component_ext, "rs_pvc", "rs_pvc", $sprite="rs_pvc")
    Lay_Right(rs_pvc_system, rs_pvc_container)
    Lay_Right(rs_pvc_container, rs_pvc_component)
    Lay_Right(rs_pvc_component, rs_pvc_system_ext)
    Lay_Right(rs_pvc_system_ext, rs_pvc_container_ext)
    Lay_Right(rs_pvc_container_ext, rs_pvc_component_ext)
    Lay_Up(rs_pvc_system, jenkins_max50_system)
}

Boundary(internet_www, "internet_www") {
    System(internet_www_system, "internet_www", "internet_www", $sprite="internet_www")
    System_Ext(internet_www_system_ext, "internet_www", "internet_www", $sprite="internet_www")
    Container(internet_www_container, "internet_www", "internet_www", $sprite="internet_www")
    Container_Ext(internet_www_container_ext, "internet_www", "internet_www", $sprite="internet_www")
    Component(internet_www_component, "internet_www", "internet_www", $sprite="internet_www")
    Component_Ext(internet_www_component_ext, "internet_www", "internet_www", $sprite="internet_www")
    Lay_Right(internet_www_system, internet_www_container)
    Lay_Right(internet_www_container, internet_www_component)
    Lay_Right(internet_www_component, internet_www_system_ext)
    Lay_Right(internet_www_system_ext, internet_www_container_ext)
    Lay_Right(internet_www_container_ext, internet_www_component_ext)
    Lay_Up(internet_www_system, rs_pvc_system)
}

Boundary(ic_control_plane, "ic_control_plane") {
    System(ic_control_plane_system, "ic_control_plane", "ic_control_plane", $sprite="ic_control_plane")
    System_Ext(ic_control_plane_system_ext, "ic_control_plane", "ic_control_plane", $sprite="ic_control_plane")
    Container(ic_control_plane_container, "ic_control_plane", "ic_control_plane", $sprite="ic_control_plane")
    Container_Ext(ic_control_plane_container_ext, "ic_control_plane", "ic_control_plane", $sprite="ic_control_plane")
    Component(ic_control_plane_component, "ic_control_plane", "ic_control_plane", $sprite="ic_control_plane")
    Component_Ext(ic_control_plane_component_ext, "ic_control_plane", "ic_control_plane", $sprite="ic_control_plane")
    Lay_Right(ic_control_plane_system, ic_control_plane_container)
    Lay_Right(ic_control_plane_container, ic_control_plane_component)
    Lay_Right(ic_control_plane_component, ic_control_plane_system_ext)
    Lay_Right(ic_control_plane_system_ext, ic_control_plane_container_ext)
    Lay_Right(ic_control_plane_container_ext, ic_control_plane_component_ext)
    Lay_Up(ic_control_plane_system, internet_www_system)
}

Boundary(vagrant_ansible, "vagrant_ansible") {
    System(vagrant_ansible_system, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    System_Ext(vagrant_ansible_system_ext, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Container(vagrant_ansible_container, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Container_Ext(vagrant_ansible_container_ext, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Component(vagrant_ansible_component, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Component_Ext(vagrant_ansible_component_ext, "vagrant_ansible", "vagrant_ansible", $sprite="vagrant_ansible")
    Lay_Right(vagrant_ansible_system, vagrant_ansible_container)
    Lay_Right(vagrant_ansible_container, vagrant_ansible_component)
    Lay_Right(vagrant_ansible_component, vagrant_ansible_system_ext)
    Lay_Right(vagrant_ansible_system_ext, vagrant_ansible_container_ext)
    Lay_Right(vagrant_ansible_container_ext, vagrant_ansible_component_ext)
    Lay_Up(vagrant_ansible_system, ic_control_plane_system)
}

Boundary(browser_min50, "browser_min50") {
    System(browser_min50_system, "browser_min50", "browser_min50", $sprite="browser_min50")
    System_Ext(browser_min50_system_ext, "browser_min50", "browser_min50", $sprite="browser_min50")
    Container(browser_min50_container, "browser_min50", "browser_min50", $sprite="browser_min50")
    Container_Ext(browser_min50_container_ext, "browser_min50", "browser_min50", $sprite="browser_min50")
    Component(browser_min50_component, "browser_min50", "browser_min50", $sprite="browser_min50")
    Component_Ext(browser_min50_component_ext, "browser_min50", "browser_min50", $sprite="browser_min50")
    Lay_Right(browser_min50_system, browser_min50_container)
    Lay_Right(browser_min50_container, browser_min50_component)
    Lay_Right(browser_min50_component, browser_min50_system_ext)
    Lay_Right(browser_min50_system_ext, browser_min50_container_ext)
    Lay_Right(browser_min50_container_ext, browser_min50_component_ext)
    Lay_Up(browser_min50_system, vagrant_ansible_system)
}

Boundary(rs_ns, "rs_ns") {
    System(rs_ns_system, "rs_ns", "rs_ns", $sprite="rs_ns")
    System_Ext(rs_ns_system_ext, "rs_ns", "rs_ns", $sprite="rs_ns")
    Container(rs_ns_container, "rs_ns", "rs_ns", $sprite="rs_ns")
    Container_Ext(rs_ns_container_ext, "rs_ns", "rs_ns", $sprite="rs_ns")
    Component(rs_ns_component, "rs_ns", "rs_ns", $sprite="rs_ns")
    Component_Ext(rs_ns_component_ext, "rs_ns", "rs_ns", $sprite="rs_ns")
    Lay_Right(rs_ns_system, rs_ns_container)
    Lay_Right(rs_ns_container, rs_ns_component)
    Lay_Right(rs_ns_component, rs_ns_system_ext)
    Lay_Right(rs_ns_system_ext, rs_ns_container_ext)
    Lay_Right(rs_ns_container_ext, rs_ns_component_ext)
    Lay_Up(rs_ns_system, browser_min50_system)
}

Boundary(rs_ep, "rs_ep") {
    System(rs_ep_system, "rs_ep", "rs_ep", $sprite="rs_ep")
    System_Ext(rs_ep_system_ext, "rs_ep", "rs_ep", $sprite="rs_ep")
    Container(rs_ep_container, "rs_ep", "rs_ep", $sprite="rs_ep")
    Container_Ext(rs_ep_container_ext, "rs_ep", "rs_ep", $sprite="rs_ep")
    Component(rs_ep_component, "rs_ep", "rs_ep", $sprite="rs_ep")
    Component_Ext(rs_ep_component_ext, "rs_ep", "rs_ep", $sprite="rs_ep")
    Lay_Right(rs_ep_system, rs_ep_container)
    Lay_Right(rs_ep_container, rs_ep_component)
    Lay_Right(rs_ep_component, rs_ep_system_ext)
    Lay_Right(rs_ep_system_ext, rs_ep_container_ext)
    Lay_Right(rs_ep_container_ext, rs_ep_component_ext)
    Lay_Up(rs_ep_system, rs_ns_system)
}

Boundary(rs_vol, "rs_vol") {
    System(rs_vol_system, "rs_vol", "rs_vol", $sprite="rs_vol")
    System_Ext(rs_vol_system_ext, "rs_vol", "rs_vol", $sprite="rs_vol")
    Container(rs_vol_container, "rs_vol", "rs_vol", $sprite="rs_vol")
    Container_Ext(rs_vol_container_ext, "rs_vol", "rs_vol", $sprite="rs_vol")
    Component(rs_vol_component, "rs_vol", "rs_vol", $sprite="rs_vol")
    Component_Ext(rs_vol_component_ext, "rs_vol", "rs_vol", $sprite="rs_vol")
    Lay_Right(rs_vol_system, rs_vol_container)
    Lay_Right(rs_vol_container, rs_vol_component)
    Lay_Right(rs_vol_component, rs_vol_system_ext)
    Lay_Right(rs_vol_system_ext, rs_vol_container_ext)
    Lay_Right(rs_vol_container_ext, rs_vol_component_ext)
    Lay_Up(rs_vol_system, rs_ep_system)
}

Boundary(fme_flow, "fme_flow") {
    System(fme_flow_system, "fme_flow", "fme_flow", $sprite="fme_flow")
    System_Ext(fme_flow_system_ext, "fme_flow", "fme_flow", $sprite="fme_flow")
    Container(fme_flow_container, "fme_flow", "fme_flow", $sprite="fme_flow")
    Container_Ext(fme_flow_container_ext, "fme_flow", "fme_flow", $sprite="fme_flow")
    Component(fme_flow_component, "fme_flow", "fme_flow", $sprite="fme_flow")
    Component_Ext(fme_flow_component_ext, "fme_flow", "fme_flow", $sprite="fme_flow")
    Lay_Right(fme_flow_system, fme_flow_container)
    Lay_Right(fme_flow_container, fme_flow_component)
    Lay_Right(fme_flow_component, fme_flow_system_ext)
    Lay_Right(fme_flow_system_ext, fme_flow_container_ext)
    Lay_Right(fme_flow_container_ext, fme_flow_component_ext)
    Lay_Up(fme_flow_system, rs_vol_system)
}

Boundary(rs_rs, "rs_rs") {
    System(rs_rs_system, "rs_rs", "rs_rs", $sprite="rs_rs")
    System_Ext(rs_rs_system_ext, "rs_rs", "rs_rs", $sprite="rs_rs")
    Container(rs_rs_container, "rs_rs", "rs_rs", $sprite="rs_rs")
    Container_Ext(rs_rs_container_ext, "rs_rs", "rs_rs", $sprite="rs_rs")
    Component(rs_rs_component, "rs_rs", "rs_rs", $sprite="rs_rs")
    Component_Ext(rs_rs_component_ext, "rs_rs", "rs_rs", $sprite="rs_rs")
    Lay_Right(rs_rs_system, rs_rs_container)
    Lay_Right(rs_rs_container, rs_rs_component)
    Lay_Right(rs_rs_component, rs_rs_system_ext)
    Lay_Right(rs_rs_system_ext, rs_rs_container_ext)
    Lay_Right(rs_rs_container_ext, rs_rs_component_ext)
    Lay_Up(rs_rs_system, fme_flow_system)
}

Boundary(rs_sc, "rs_sc") {
    System(rs_sc_system, "rs_sc", "rs_sc", $sprite="rs_sc")
    System_Ext(rs_sc_system_ext, "rs_sc", "rs_sc", $sprite="rs_sc")
    Container(rs_sc_container, "rs_sc", "rs_sc", $sprite="rs_sc")
    Container_Ext(rs_sc_container_ext, "rs_sc", "rs_sc", $sprite="rs_sc")
    Component(rs_sc_component, "rs_sc", "rs_sc", $sprite="rs_sc")
    Component_Ext(rs_sc_component_ext, "rs_sc", "rs_sc", $sprite="rs_sc")
    Lay_Right(rs_sc_system, rs_sc_container)
    Lay_Right(rs_sc_container, rs_sc_component)
    Lay_Right(rs_sc_component, rs_sc_system_ext)
    Lay_Right(rs_sc_system_ext, rs_sc_container_ext)
    Lay_Right(rs_sc_container_ext, rs_sc_component_ext)
    Lay_Up(rs_sc_system, rs_rs_system)
}

Boundary(envoy_max50, "envoy_max50") {
    System(envoy_max50_system, "envoy_max50", "envoy_max50", $sprite="envoy_max50")
    System_Ext(envoy_max50_system_ext, "envoy_max50", "envoy_max50", $sprite="envoy_max50")
    Container(envoy_max50_container, "envoy_max50", "envoy_max50", $sprite="envoy_max50")
    Container_Ext(envoy_max50_container_ext, "envoy_max50", "envoy_max50", $sprite="envoy_max50")
    Component(envoy_max50_component, "envoy_max50", "envoy_max50", $sprite="envoy_max50")
    Component_Ext(envoy_max50_component_ext, "envoy_max50", "envoy_max50", $sprite="envoy_max50")
    Lay_Right(envoy_max50_system, envoy_max50_container)
    Lay_Right(envoy_max50_container, envoy_max50_component)
    Lay_Right(envoy_max50_component, envoy_max50_system_ext)
    Lay_Right(envoy_max50_system_ext, envoy_max50_container_ext)
    Lay_Right(envoy_max50_container_ext, envoy_max50_component_ext)
    Lay_Up(envoy_max50_system, rs_sc_system)
}

Boundary(rs_sa, "rs_sa") {
    System(rs_sa_system, "rs_sa", "rs_sa", $sprite="rs_sa")
    System_Ext(rs_sa_system_ext, "rs_sa", "rs_sa", $sprite="rs_sa")
    Container(rs_sa_container, "rs_sa", "rs_sa", $sprite="rs_sa")
    Container_Ext(rs_sa_container_ext, "rs_sa", "rs_sa", $sprite="rs_sa")
    Component(rs_sa_component, "rs_sa", "rs_sa", $sprite="rs_sa")
    Component_Ext(rs_sa_component_ext, "rs_sa", "rs_sa", $sprite="rs_sa")
    Lay_Right(rs_sa_system, rs_sa_container)
    Lay_Right(rs_sa_container, rs_sa_component)
    Lay_Right(rs_sa_component, rs_sa_system_ext)
    Lay_Right(rs_sa_system_ext, rs_sa_container_ext)
    Lay_Right(rs_sa_container_ext, rs_sa_component_ext)
    Lay_Up(rs_sa_system, envoy_max50_system)
}

Boundary(docker_max50, "docker_max50") {
    System(docker_max50_system, "docker_max50", "docker_max50", $sprite="docker_max50")
    System_Ext(docker_max50_system_ext, "docker_max50", "docker_max50", $sprite="docker_max50")
    Container(docker_max50_container, "docker_max50", "docker_max50", $sprite="docker_max50")
    Container_Ext(docker_max50_container_ext, "docker_max50", "docker_max50", $sprite="docker_max50")
    Component(docker_max50_component, "docker_max50", "docker_max50", $sprite="docker_max50")
    Component_Ext(docker_max50_component_ext, "docker_max50", "docker_max50", $sprite="docker_max50")
    Lay_Right(docker_max50_system, docker_max50_container)
    Lay_Right(docker_max50_container, docker_max50_component)
    Lay_Right(docker_max50_component, docker_max50_system_ext)
    Lay_Right(docker_max50_system_ext, docker_max50_container_ext)
    Lay_Right(docker_max50_container_ext, docker_max50_component_ext)
    Lay_Up(docker_max50_system, rs_sa_system)
}

Boundary(azure, "azure") {
    System(azure_system, "azure", "azure", $sprite="azure")
    System_Ext(azure_system_ext, "azure", "azure", $sprite="azure")
    Container(azure_container, "azure", "azure", $sprite="azure")
    Container_Ext(azure_container_ext, "azure", "azure", $sprite="azure")
    Component(azure_component, "azure", "azure", $sprite="azure")
    Component_Ext(azure_component_ext, "azure", "azure", $sprite="azure")
    Lay_Right(azure_system, azure_container)
    Lay_Right(azure_container, azure_component)
    Lay_Right(azure_component, azure_system_ext)
    Lay_Right(azure_system_ext, azure_container_ext)
    Lay_Right(azure_container_ext, azure_component_ext)
    Lay_Up(azure_system, docker_max50_system)
}

Boundary(argocd, "argocd") {
    System(argocd_system, "argocd", "argocd", $sprite="argocd")
    System_Ext(argocd_system_ext, "argocd", "argocd", $sprite="argocd")
    Container(argocd_container, "argocd", "argocd", $sprite="argocd")
    Container_Ext(argocd_container_ext, "argocd", "argocd", $sprite="argocd")
    Component(argocd_component, "argocd", "argocd", $sprite="argocd")
    Component_Ext(argocd_component_ext, "argocd", "argocd", $sprite="argocd")
    Lay_Right(argocd_system, argocd_container)
    Lay_Right(argocd_container, argocd_component)
    Lay_Right(argocd_component, argocd_system_ext)
    Lay_Right(argocd_system_ext, argocd_container_ext)
    Lay_Right(argocd_container_ext, argocd_component_ext)
    Lay_Up(argocd_system, azure_system)
}

Boundary(awx_min50, "awx_min50") {
    System(awx_min50_system, "awx_min50", "awx_min50", $sprite="awx_min50")
    System_Ext(awx_min50_system_ext, "awx_min50", "awx_min50", $sprite="awx_min50")
    Container(awx_min50_container, "awx_min50", "awx_min50", $sprite="awx_min50")
    Container_Ext(awx_min50_container_ext, "awx_min50", "awx_min50", $sprite="awx_min50")
    Component(awx_min50_component, "awx_min50", "awx_min50", $sprite="awx_min50")
    Component_Ext(awx_min50_component_ext, "awx_min50", "awx_min50", $sprite="awx_min50")
    Lay_Right(awx_min50_system, awx_min50_container)
    Lay_Right(awx_min50_container, awx_min50_component)
    Lay_Right(awx_min50_component, awx_min50_system_ext)
    Lay_Right(awx_min50_system_ext, awx_min50_container_ext)
    Lay_Right(awx_min50_container_ext, awx_min50_component_ext)
    Lay_Up(awx_min50_system, argocd_system)
}

Boundary(vagrant_plus_ansible_max50, "vagrant_plus_ansible_max50") {
    System(vagrant_plus_ansible_max50_system, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    System_Ext(vagrant_plus_ansible_max50_system_ext, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Container(vagrant_plus_ansible_max50_container, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Container_Ext(vagrant_plus_ansible_max50_container_ext, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Component(vagrant_plus_ansible_max50_component, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Component_Ext(vagrant_plus_ansible_max50_component_ext, "vagrant_plus_ansible_max50", "vagrant_plus_ansible_max50", $sprite="vagrant_plus_ansible_max50")
    Lay_Right(vagrant_plus_ansible_max50_system, vagrant_plus_ansible_max50_container)
    Lay_Right(vagrant_plus_ansible_max50_container, vagrant_plus_ansible_max50_component)
    Lay_Right(vagrant_plus_ansible_max50_component, vagrant_plus_ansible_max50_system_ext)
    Lay_Right(vagrant_plus_ansible_max50_system_ext, vagrant_plus_ansible_max50_container_ext)
    Lay_Right(vagrant_plus_ansible_max50_container_ext, vagrant_plus_ansible_max50_component_ext)
    Lay_Up(vagrant_plus_ansible_max50_system, awx_min50_system)
}

Boundary(vagrant_plus_ansible_min50, "vagrant_plus_ansible_min50") {
    System(vagrant_plus_ansible_min50_system, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    System_Ext(vagrant_plus_ansible_min50_system_ext, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Container(vagrant_plus_ansible_min50_container, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Container_Ext(vagrant_plus_ansible_min50_container_ext, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Component(vagrant_plus_ansible_min50_component, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Component_Ext(vagrant_plus_ansible_min50_component_ext, "vagrant_plus_ansible_min50", "vagrant_plus_ansible_min50", $sprite="vagrant_plus_ansible_min50")
    Lay_Right(vagrant_plus_ansible_min50_system, vagrant_plus_ansible_min50_container)
    Lay_Right(vagrant_plus_ansible_min50_container, vagrant_plus_ansible_min50_component)
    Lay_Right(vagrant_plus_ansible_min50_component, vagrant_plus_ansible_min50_system_ext)
    Lay_Right(vagrant_plus_ansible_min50_system_ext, vagrant_plus_ansible_min50_container_ext)
    Lay_Right(vagrant_plus_ansible_min50_container_ext, vagrant_plus_ansible_min50_component_ext)
    Lay_Up(vagrant_plus_ansible_min50_system, vagrant_plus_ansible_max50_system)
}

Boundary(rs_cronjob, "rs_cronjob") {
    System(rs_cronjob_system, "rs_cronjob", "rs_cronjob", $sprite="rs_cronjob")
    System_Ext(rs_cronjob_system_ext, "rs_cronjob", "rs_cronjob", $sprite="rs_cronjob")
    Container(rs_cronjob_container, "rs_cronjob", "rs_cronjob", $sprite="rs_cronjob")
    Container_Ext(rs_cronjob_container_ext, "rs_cronjob", "rs_cronjob", $sprite="rs_cronjob")
    Component(rs_cronjob_component, "rs_cronjob", "rs_cronjob", $sprite="rs_cronjob")
    Component_Ext(rs_cronjob_component_ext, "rs_cronjob", "rs_cronjob", $sprite="rs_cronjob")
    Lay_Right(rs_cronjob_system, rs_cronjob_container)
    Lay_Right(rs_cronjob_container, rs_cronjob_component)
    Lay_Right(rs_cronjob_component, rs_cronjob_system_ext)
    Lay_Right(rs_cronjob_system_ext, rs_cronjob_container_ext)
    Lay_Right(rs_cronjob_container_ext, rs_cronjob_component_ext)
    Lay_Up(rs_cronjob_system, vagrant_plus_ansible_min50_system)
}

Boundary(haproxy, "haproxy") {
    System(haproxy_system, "haproxy", "haproxy", $sprite="haproxy")
    System_Ext(haproxy_system_ext, "haproxy", "haproxy", $sprite="haproxy")
    Container(haproxy_container, "haproxy", "haproxy", $sprite="haproxy")
    Container_Ext(haproxy_container_ext, "haproxy", "haproxy", $sprite="haproxy")
    Component(haproxy_component, "haproxy", "haproxy", $sprite="haproxy")
    Component_Ext(haproxy_component_ext, "haproxy", "haproxy", $sprite="haproxy")
    Lay_Right(haproxy_system, haproxy_container)
    Lay_Right(haproxy_container, haproxy_component)
    Lay_Right(haproxy_component, haproxy_system_ext)
    Lay_Right(haproxy_system_ext, haproxy_container_ext)
    Lay_Right(haproxy_container_ext, haproxy_component_ext)
    Lay_Up(haproxy_system, rs_cronjob_system)
}

Boundary(lxc, "lxc") {
    System(lxc_system, "lxc", "lxc", $sprite="lxc")
    System_Ext(lxc_system_ext, "lxc", "lxc", $sprite="lxc")
    Container(lxc_container, "lxc", "lxc", $sprite="lxc")
    Container_Ext(lxc_container_ext, "lxc", "lxc", $sprite="lxc")
    Component(lxc_component, "lxc", "lxc", $sprite="lxc")
    Component_Ext(lxc_component_ext, "lxc", "lxc", $sprite="lxc")
    Lay_Right(lxc_system, lxc_container)
    Lay_Right(lxc_container, lxc_component)
    Lay_Right(lxc_component, lxc_system_ext)
    Lay_Right(lxc_system_ext, lxc_container_ext)
    Lay_Right(lxc_container_ext, lxc_component_ext)
    Lay_Up(lxc_system, haproxy_system)
}

Boundary(gateway, "gateway") {
    System(gateway_system, "gateway", "gateway", $sprite="gateway")
    System_Ext(gateway_system_ext, "gateway", "gateway", $sprite="gateway")
    Container(gateway_container, "gateway", "gateway", $sprite="gateway")
    Container_Ext(gateway_container_ext, "gateway", "gateway", $sprite="gateway")
    Component(gateway_component, "gateway", "gateway", $sprite="gateway")
    Component_Ext(gateway_component_ext, "gateway", "gateway", $sprite="gateway")
    Lay_Right(gateway_system, gateway_container)
    Lay_Right(gateway_container, gateway_component)
    Lay_Right(gateway_component, gateway_system_ext)
    Lay_Right(gateway_system_ext, gateway_container_ext)
    Lay_Right(gateway_container_ext, gateway_component_ext)
    Lay_Up(gateway_system, lxc_system)
}

Boundary(rs_job, "rs_job") {
    System(rs_job_system, "rs_job", "rs_job", $sprite="rs_job")
    System_Ext(rs_job_system_ext, "rs_job", "rs_job", $sprite="rs_job")
    Container(rs_job_container, "rs_job", "rs_job", $sprite="rs_job")
    Container_Ext(rs_job_container_ext, "rs_job", "rs_job", $sprite="rs_job")
    Component(rs_job_component, "rs_job", "rs_job", $sprite="rs_job")
    Component_Ext(rs_job_component_ext, "rs_job", "rs_job", $sprite="rs_job")
    Lay_Right(rs_job_system, rs_job_container)
    Lay_Right(rs_job_container, rs_job_component)
    Lay_Right(rs_job_component, rs_job_system_ext)
    Lay_Right(rs_job_system_ext, rs_job_container_ext)
    Lay_Right(rs_job_container_ext, rs_job_component_ext)
    Lay_Up(rs_job_system, gateway_system)
}

Boundary(ic_etcd, "ic_etcd") {
    System(ic_etcd_system, "ic_etcd", "ic_etcd", $sprite="ic_etcd")
    System_Ext(ic_etcd_system_ext, "ic_etcd", "ic_etcd", $sprite="ic_etcd")
    Container(ic_etcd_container, "ic_etcd", "ic_etcd", $sprite="ic_etcd")
    Container_Ext(ic_etcd_container_ext, "ic_etcd", "ic_etcd", $sprite="ic_etcd")
    Component(ic_etcd_component, "ic_etcd", "ic_etcd", $sprite="ic_etcd")
    Component_Ext(ic_etcd_component_ext, "ic_etcd", "ic_etcd", $sprite="ic_etcd")
    Lay_Right(ic_etcd_system, ic_etcd_container)
    Lay_Right(ic_etcd_container, ic_etcd_component)
    Lay_Right(ic_etcd_component, ic_etcd_system_ext)
    Lay_Right(ic_etcd_system_ext, ic_etcd_container_ext)
    Lay_Right(ic_etcd_container_ext, ic_etcd_component_ext)
    Lay_Up(ic_etcd_system, rs_job_system)
}

Boundary(docker_min50, "docker_min50") {
    System(docker_min50_system, "docker_min50", "docker_min50", $sprite="docker_min50")
    System_Ext(docker_min50_system_ext, "docker_min50", "docker_min50", $sprite="docker_min50")
    Container(docker_min50_container, "docker_min50", "docker_min50", $sprite="docker_min50")
    Container_Ext(docker_min50_container_ext, "docker_min50", "docker_min50", $sprite="docker_min50")
    Component(docker_min50_component, "docker_min50", "docker_min50", $sprite="docker_min50")
    Component_Ext(docker_min50_component_ext, "docker_min50", "docker_min50", $sprite="docker_min50")
    Lay_Right(docker_min50_system, docker_min50_container)
    Lay_Right(docker_min50_container, docker_min50_component)
    Lay_Right(docker_min50_component, docker_min50_system_ext)
    Lay_Right(docker_min50_system_ext, docker_min50_container_ext)
    Lay_Right(docker_min50_container_ext, docker_min50_component_ext)
    Lay_Up(docker_min50_system, ic_etcd_system)
}

Boundary(bkwi, "bkwi") {
    System(bkwi_system, "bkwi", "bkwi", $sprite="bkwi")
    System_Ext(bkwi_system_ext, "bkwi", "bkwi", $sprite="bkwi")
    Container(bkwi_container, "bkwi", "bkwi", $sprite="bkwi")
    Container_Ext(bkwi_container_ext, "bkwi", "bkwi", $sprite="bkwi")
    Component(bkwi_component, "bkwi", "bkwi", $sprite="bkwi")
    Component_Ext(bkwi_component_ext, "bkwi", "bkwi", $sprite="bkwi")
    Lay_Right(bkwi_system, bkwi_container)
    Lay_Right(bkwi_container, bkwi_component)
    Lay_Right(bkwi_component, bkwi_system_ext)
    Lay_Right(bkwi_system_ext, bkwi_container_ext)
    Lay_Right(bkwi_container_ext, bkwi_component_ext)
    Lay_Up(bkwi_system, docker_min50_system)
}

Boundary(ide_max50, "ide_max50") {
    System(ide_max50_system, "ide_max50", "ide_max50", $sprite="ide_max50")
    System_Ext(ide_max50_system_ext, "ide_max50", "ide_max50", $sprite="ide_max50")
    Container(ide_max50_container, "ide_max50", "ide_max50", $sprite="ide_max50")
    Container_Ext(ide_max50_container_ext, "ide_max50", "ide_max50", $sprite="ide_max50")
    Component(ide_max50_component, "ide_max50", "ide_max50", $sprite="ide_max50")
    Component_Ext(ide_max50_component_ext, "ide_max50", "ide_max50", $sprite="ide_max50")
    Lay_Right(ide_max50_system, ide_max50_container)
    Lay_Right(ide_max50_container, ide_max50_component)
    Lay_Right(ide_max50_component, ide_max50_system_ext)
    Lay_Right(ide_max50_system_ext, ide_max50_container_ext)
    Lay_Right(ide_max50_container_ext, ide_max50_component_ext)
    Lay_Up(ide_max50_system, bkwi_system)
}

Boundary(fme_form, "fme_form") {
    System(fme_form_system, "fme_form", "fme_form", $sprite="fme_form")
    System_Ext(fme_form_system_ext, "fme_form", "fme_form", $sprite="fme_form")
    Container(fme_form_container, "fme_form", "fme_form", $sprite="fme_form")
    Container_Ext(fme_form_container_ext, "fme_form", "fme_form", $sprite="fme_form")
    Component(fme_form_component, "fme_form", "fme_form", $sprite="fme_form")
    Component_Ext(fme_form_component_ext, "fme_form", "fme_form", $sprite="fme_form")
    Lay_Right(fme_form_system, fme_form_container)
    Lay_Right(fme_form_container, fme_form_component)
    Lay_Right(fme_form_component, fme_form_system_ext)
    Lay_Right(fme_form_system_ext, fme_form_container_ext)
    Lay_Right(fme_form_container_ext, fme_form_component_ext)
    Lay_Up(fme_form_system, ide_max50_system)
}

Boundary(fme_flow_hosted, "fme_flow_hosted") {
    System(fme_flow_hosted_system, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    System_Ext(fme_flow_hosted_system_ext, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Container(fme_flow_hosted_container, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Container_Ext(fme_flow_hosted_container_ext, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Component(fme_flow_hosted_component, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Component_Ext(fme_flow_hosted_component_ext, "fme_flow_hosted", "fme_flow_hosted", $sprite="fme_flow_hosted")
    Lay_Right(fme_flow_hosted_system, fme_flow_hosted_container)
    Lay_Right(fme_flow_hosted_container, fme_flow_hosted_component)
    Lay_Right(fme_flow_hosted_component, fme_flow_hosted_system_ext)
    Lay_Right(fme_flow_hosted_system_ext, fme_flow_hosted_container_ext)
    Lay_Right(fme_flow_hosted_container_ext, fme_flow_hosted_component_ext)
    Lay_Up(fme_flow_hosted_system, fme_form_system)
}

Boundary(rs_crb, "rs_crb") {
    System(rs_crb_system, "rs_crb", "rs_crb", $sprite="rs_crb")
    System_Ext(rs_crb_system_ext, "rs_crb", "rs_crb", $sprite="rs_crb")
    Container(rs_crb_container, "rs_crb", "rs_crb", $sprite="rs_crb")
    Container_Ext(rs_crb_container_ext, "rs_crb", "rs_crb", $sprite="rs_crb")
    Component(rs_crb_component, "rs_crb", "rs_crb", $sprite="rs_crb")
    Component_Ext(rs_crb_component_ext, "rs_crb", "rs_crb", $sprite="rs_crb")
    Lay_Right(rs_crb_system, rs_crb_container)
    Lay_Right(rs_crb_container, rs_crb_component)
    Lay_Right(rs_crb_component, rs_crb_system_ext)
    Lay_Right(rs_crb_system_ext, rs_crb_container_ext)
    Lay_Right(rs_crb_container_ext, rs_crb_component_ext)
    Lay_Up(rs_crb_system, fme_flow_hosted_system)
}

Boundary(rs_netpol, "rs_netpol") {
    System(rs_netpol_system, "rs_netpol", "rs_netpol", $sprite="rs_netpol")
    System_Ext(rs_netpol_system_ext, "rs_netpol", "rs_netpol", $sprite="rs_netpol")
    Container(rs_netpol_container, "rs_netpol", "rs_netpol", $sprite="rs_netpol")
    Container_Ext(rs_netpol_container_ext, "rs_netpol", "rs_netpol", $sprite="rs_netpol")
    Component(rs_netpol_component, "rs_netpol", "rs_netpol", $sprite="rs_netpol")
    Component_Ext(rs_netpol_component_ext, "rs_netpol", "rs_netpol", $sprite="rs_netpol")
    Lay_Right(rs_netpol_system, rs_netpol_container)
    Lay_Right(rs_netpol_container, rs_netpol_component)
    Lay_Right(rs_netpol_component, rs_netpol_system_ext)
    Lay_Right(rs_netpol_system_ext, rs_netpol_container_ext)
    Lay_Right(rs_netpol_container_ext, rs_netpol_component_ext)
    Lay_Up(rs_netpol_system, rs_crb_system)
}

Boundary(rs_secret, "rs_secret") {
    System(rs_secret_system, "rs_secret", "rs_secret", $sprite="rs_secret")
    System_Ext(rs_secret_system_ext, "rs_secret", "rs_secret", $sprite="rs_secret")
    Container(rs_secret_container, "rs_secret", "rs_secret", $sprite="rs_secret")
    Container_Ext(rs_secret_container_ext, "rs_secret", "rs_secret", $sprite="rs_secret")
    Component(rs_secret_component, "rs_secret", "rs_secret", $sprite="rs_secret")
    Component_Ext(rs_secret_component_ext, "rs_secret", "rs_secret", $sprite="rs_secret")
    Lay_Right(rs_secret_system, rs_secret_container)
    Lay_Right(rs_secret_container, rs_secret_component)
    Lay_Right(rs_secret_component, rs_secret_system_ext)
    Lay_Right(rs_secret_system_ext, rs_secret_container_ext)
    Lay_Right(rs_secret_container_ext, rs_secret_component_ext)
    Lay_Up(rs_secret_system, rs_netpol_system)
}

Boundary(envoy_min50, "envoy_min50") {
    System(envoy_min50_system, "envoy_min50", "envoy_min50", $sprite="envoy_min50")
    System_Ext(envoy_min50_system_ext, "envoy_min50", "envoy_min50", $sprite="envoy_min50")
    Container(envoy_min50_container, "envoy_min50", "envoy_min50", $sprite="envoy_min50")
    Container_Ext(envoy_min50_container_ext, "envoy_min50", "envoy_min50", $sprite="envoy_min50")
    Component(envoy_min50_component, "envoy_min50", "envoy_min50", $sprite="envoy_min50")
    Component_Ext(envoy_min50_component_ext, "envoy_min50", "envoy_min50", $sprite="envoy_min50")
    Lay_Right(envoy_min50_system, envoy_min50_container)
    Lay_Right(envoy_min50_container, envoy_min50_component)
    Lay_Right(envoy_min50_component, envoy_min50_system_ext)
    Lay_Right(envoy_min50_system_ext, envoy_min50_container_ext)
    Lay_Right(envoy_min50_container_ext, envoy_min50_component_ext)
    Lay_Up(envoy_min50_system, rs_secret_system)
}

Boundary(rs_pod, "rs_pod") {
    System(rs_pod_system, "rs_pod", "rs_pod", $sprite="rs_pod")
    System_Ext(rs_pod_system_ext, "rs_pod", "rs_pod", $sprite="rs_pod")
    Container(rs_pod_container, "rs_pod", "rs_pod", $sprite="rs_pod")
    Container_Ext(rs_pod_container_ext, "rs_pod", "rs_pod", $sprite="rs_pod")
    Component(rs_pod_component, "rs_pod", "rs_pod", $sprite="rs_pod")
    Component_Ext(rs_pod_component_ext, "rs_pod", "rs_pod", $sprite="rs_pod")
    Lay_Right(rs_pod_system, rs_pod_container)
    Lay_Right(rs_pod_container, rs_pod_component)
    Lay_Right(rs_pod_component, rs_pod_system_ext)
    Lay_Right(rs_pod_system_ext, rs_pod_container_ext)
    Lay_Right(rs_pod_container_ext, rs_pod_component_ext)
    Lay_Up(rs_pod_system, envoy_min50_system)
}

Boundary(rs_psp, "rs_psp") {
    System(rs_psp_system, "rs_psp", "rs_psp", $sprite="rs_psp")
    System_Ext(rs_psp_system_ext, "rs_psp", "rs_psp", $sprite="rs_psp")
    Container(rs_psp_container, "rs_psp", "rs_psp", $sprite="rs_psp")
    Container_Ext(rs_psp_container_ext, "rs_psp", "rs_psp", $sprite="rs_psp")
    Component(rs_psp_component, "rs_psp", "rs_psp", $sprite="rs_psp")
    Component_Ext(rs_psp_component_ext, "rs_psp", "rs_psp", $sprite="rs_psp")
    Lay_Right(rs_psp_system, rs_psp_container)
    Lay_Right(rs_psp_container, rs_psp_component)
    Lay_Right(rs_psp_component, rs_psp_system_ext)
    Lay_Right(rs_psp_system_ext, rs_psp_container_ext)
    Lay_Right(rs_psp_container_ext, rs_psp_component_ext)
    Lay_Up(rs_psp_system, rs_pod_system)
}

Boundary(internet2, "internet2") {
    System(internet2_system, "internet2", "internet2", $sprite="internet2")
    System_Ext(internet2_system_ext, "internet2", "internet2", $sprite="internet2")
    Container(internet2_container, "internet2", "internet2", $sprite="internet2")
    Container_Ext(internet2_container_ext, "internet2", "internet2", $sprite="internet2")
    Component(internet2_component, "internet2", "internet2", $sprite="internet2")
    Component_Ext(internet2_component_ext, "internet2", "internet2", $sprite="internet2")
    Lay_Right(internet2_system, internet2_container)
    Lay_Right(internet2_container, internet2_component)
    Lay_Right(internet2_component, internet2_system_ext)
    Lay_Right(internet2_system_ext, internet2_container_ext)
    Lay_Right(internet2_container_ext, internet2_component_ext)
    Lay_Up(internet2_system, rs_psp_system)
}

Boundary(rs_hpa, "rs_hpa") {
    System(rs_hpa_system, "rs_hpa", "rs_hpa", $sprite="rs_hpa")
    System_Ext(rs_hpa_system_ext, "rs_hpa", "rs_hpa", $sprite="rs_hpa")
    Container(rs_hpa_container, "rs_hpa", "rs_hpa", $sprite="rs_hpa")
    Container_Ext(rs_hpa_container_ext, "rs_hpa", "rs_hpa", $sprite="rs_hpa")
    Component(rs_hpa_component, "rs_hpa", "rs_hpa", $sprite="rs_hpa")
    Component_Ext(rs_hpa_component_ext, "rs_hpa", "rs_hpa", $sprite="rs_hpa")
    Lay_Right(rs_hpa_system, rs_hpa_container)
    Lay_Right(rs_hpa_container, rs_hpa_component)
    Lay_Right(rs_hpa_component, rs_hpa_system_ext)
    Lay_Right(rs_hpa_system_ext, rs_hpa_container_ext)
    Lay_Right(rs_hpa_container_ext, rs_hpa_component_ext)
    Lay_Up(rs_hpa_system, internet2_system)
}

Boundary(internet3, "internet3") {
    System(internet3_system, "internet3", "internet3", $sprite="internet3")
    System_Ext(internet3_system_ext, "internet3", "internet3", $sprite="internet3")
    Container(internet3_container, "internet3", "internet3", $sprite="internet3")
    Container_Ext(internet3_container_ext, "internet3", "internet3", $sprite="internet3")
    Component(internet3_component, "internet3", "internet3", $sprite="internet3")
    Component_Ext(internet3_component_ext, "internet3", "internet3", $sprite="internet3")
    Lay_Right(internet3_system, internet3_container)
    Lay_Right(internet3_container, internet3_component)
    Lay_Right(internet3_component, internet3_system_ext)
    Lay_Right(internet3_system_ext, internet3_container_ext)
    Lay_Right(internet3_container_ext, internet3_component_ext)
    Lay_Up(internet3_system, rs_hpa_system)
}

Boundary(rs_svc, "rs_svc") {
    System(rs_svc_system, "rs_svc", "rs_svc", $sprite="rs_svc")
    System_Ext(rs_svc_system_ext, "rs_svc", "rs_svc", $sprite="rs_svc")
    Container(rs_svc_container, "rs_svc", "rs_svc", $sprite="rs_svc")
    Container_Ext(rs_svc_container_ext, "rs_svc", "rs_svc", $sprite="rs_svc")
    Component(rs_svc_component, "rs_svc", "rs_svc", $sprite="rs_svc")
    Component_Ext(rs_svc_component_ext, "rs_svc", "rs_svc", $sprite="rs_svc")
    Lay_Right(rs_svc_system, rs_svc_container)
    Lay_Right(rs_svc_container, rs_svc_component)
    Lay_Right(rs_svc_component, rs_svc_system_ext)
    Lay_Right(rs_svc_system_ext, rs_svc_container_ext)
    Lay_Right(rs_svc_container_ext, rs_svc_component_ext)
    Lay_Up(rs_svc_system, internet3_system)
}

Boundary(jenkins_min50, "jenkins_min50") {
    System(jenkins_min50_system, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    System_Ext(jenkins_min50_system_ext, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Container(jenkins_min50_container, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Container_Ext(jenkins_min50_container_ext, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Component(jenkins_min50_component, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Component_Ext(jenkins_min50_component_ext, "jenkins_min50", "jenkins_min50", $sprite="jenkins_min50")
    Lay_Right(jenkins_min50_system, jenkins_min50_container)
    Lay_Right(jenkins_min50_container, jenkins_min50_component)
    Lay_Right(jenkins_min50_component, jenkins_min50_system_ext)
    Lay_Right(jenkins_min50_system_ext, jenkins_min50_container_ext)
    Lay_Right(jenkins_min50_container_ext, jenkins_min50_component_ext)
    Lay_Up(jenkins_min50_system, rs_svc_system)
}

Boundary(awx_max50, "awx_max50") {
    System(awx_max50_system, "awx_max50", "awx_max50", $sprite="awx_max50")
    System_Ext(awx_max50_system_ext, "awx_max50", "awx_max50", $sprite="awx_max50")
    Container(awx_max50_container, "awx_max50", "awx_max50", $sprite="awx_max50")
    Container_Ext(awx_max50_container_ext, "awx_max50", "awx_max50", $sprite="awx_max50")
    Component(awx_max50_component, "awx_max50", "awx_max50", $sprite="awx_max50")
    Component_Ext(awx_max50_component_ext, "awx_max50", "awx_max50", $sprite="awx_max50")
    Lay_Right(awx_max50_system, awx_max50_container)
    Lay_Right(awx_max50_container, awx_max50_component)
    Lay_Right(awx_max50_component, awx_max50_system_ext)
    Lay_Right(awx_max50_system_ext, awx_max50_container_ext)
    Lay_Right(awx_max50_container_ext, awx_max50_component_ext)
    Lay_Up(awx_max50_system, jenkins_min50_system)
}

Boundary(rs_group, "rs_group") {
    System(rs_group_system, "rs_group", "rs_group", $sprite="rs_group")
    System_Ext(rs_group_system_ext, "rs_group", "rs_group", $sprite="rs_group")
    Container(rs_group_container, "rs_group", "rs_group", $sprite="rs_group")
    Container_Ext(rs_group_container_ext, "rs_group", "rs_group", $sprite="rs_group")
    Component(rs_group_component, "rs_group", "rs_group", $sprite="rs_group")
    Component_Ext(rs_group_component_ext, "rs_group", "rs_group", $sprite="rs_group")
    Lay_Right(rs_group_system, rs_group_container)
    Lay_Right(rs_group_container, rs_group_component)
    Lay_Right(rs_group_component, rs_group_system_ext)
    Lay_Right(rs_group_system_ext, rs_group_container_ext)
    Lay_Right(rs_group_container_ext, rs_group_component_ext)
    Lay_Up(rs_group_system, awx_max50_system)
}

Boundary(laptop, "laptop") {
    System(laptop_system, "laptop", "laptop", $sprite="laptop")
    System_Ext(laptop_system_ext, "laptop", "laptop", $sprite="laptop")
    Container(laptop_container, "laptop", "laptop", $sprite="laptop")
    Container_Ext(laptop_container_ext, "laptop", "laptop", $sprite="laptop")
    Component(laptop_component, "laptop", "laptop", $sprite="laptop")
    Component_Ext(laptop_component_ext, "laptop", "laptop", $sprite="laptop")
    Lay_Right(laptop_system, laptop_container)
    Lay_Right(laptop_container, laptop_component)
    Lay_Right(laptop_component, laptop_system_ext)
    Lay_Right(laptop_system_ext, laptop_container_ext)
    Lay_Right(laptop_container_ext, laptop_component_ext)
    Lay_Up(laptop_system, rs_group_system)
}

Boundary(rs_quota, "rs_quota") {
    System(rs_quota_system, "rs_quota", "rs_quota", $sprite="rs_quota")
    System_Ext(rs_quota_system_ext, "rs_quota", "rs_quota", $sprite="rs_quota")
    Container(rs_quota_container, "rs_quota", "rs_quota", $sprite="rs_quota")
    Container_Ext(rs_quota_container_ext, "rs_quota", "rs_quota", $sprite="rs_quota")
    Component(rs_quota_component, "rs_quota", "rs_quota", $sprite="rs_quota")
    Component_Ext(rs_quota_component_ext, "rs_quota", "rs_quota", $sprite="rs_quota")
    Lay_Right(rs_quota_system, rs_quota_container)
    Lay_Right(rs_quota_container, rs_quota_component)
    Lay_Right(rs_quota_component, rs_quota_system_ext)
    Lay_Right(rs_quota_system_ext, rs_quota_container_ext)
    Lay_Right(rs_quota_container_ext, rs_quota_component_ext)
    Lay_Up(rs_quota_system, laptop_system)
}

Boundary(rs_crd, "rs_crd") {
    System(rs_crd_system, "rs_crd", "rs_crd", $sprite="rs_crd")
    System_Ext(rs_crd_system_ext, "rs_crd", "rs_crd", $sprite="rs_crd")
    Container(rs_crd_container, "rs_crd", "rs_crd", $sprite="rs_crd")
    Container_Ext(rs_crd_container_ext, "rs_crd", "rs_crd", $sprite="rs_crd")
    Component(rs_crd_component, "rs_crd", "rs_crd", $sprite="rs_crd")
    Component_Ext(rs_crd_component_ext, "rs_crd", "rs_crd", $sprite="rs_crd")
    Lay_Right(rs_crd_system, rs_crd_container)
    Lay_Right(rs_crd_container, rs_crd_component)
    Lay_Right(rs_crd_component, rs_crd_system_ext)
    Lay_Right(rs_crd_system_ext, rs_crd_container_ext)
    Lay_Right(rs_crd_container_ext, rs_crd_component_ext)
    Lay_Up(rs_crd_system, rs_quota_system)
}

Boundary(politie_min50, "politie_min50") {
    System(politie_min50_system, "politie_min50", "politie_min50", $sprite="politie_min50")
    System_Ext(politie_min50_system_ext, "politie_min50", "politie_min50", $sprite="politie_min50")
    Container(politie_min50_container, "politie_min50", "politie_min50", $sprite="politie_min50")
    Container_Ext(politie_min50_container_ext, "politie_min50", "politie_min50", $sprite="politie_min50")
    Component(politie_min50_component, "politie_min50", "politie_min50", $sprite="politie_min50")
    Component_Ext(politie_min50_component_ext, "politie_min50", "politie_min50", $sprite="politie_min50")
    Lay_Right(politie_min50_system, politie_min50_container)
    Lay_Right(politie_min50_container, politie_min50_component)
    Lay_Right(politie_min50_component, politie_min50_system_ext)
    Lay_Right(politie_min50_system_ext, politie_min50_container_ext)
    Lay_Right(politie_min50_container_ext, politie_min50_component_ext)
    Lay_Up(politie_min50_system, rs_crd_system)
}

Boundary(politie_max50, "politie_max50") {
    System(politie_max50_system, "politie_max50", "politie_max50", $sprite="politie_max50")
    System_Ext(politie_max50_system_ext, "politie_max50", "politie_max50", $sprite="politie_max50")
    Container(politie_max50_container, "politie_max50", "politie_max50", $sprite="politie_max50")
    Container_Ext(politie_max50_container_ext, "politie_max50", "politie_max50", $sprite="politie_max50")
    Component(politie_max50_component, "politie_max50", "politie_max50", $sprite="politie_max50")
    Component_Ext(politie_max50_component_ext, "politie_max50", "politie_max50", $sprite="politie_max50")
    Lay_Right(politie_max50_system, politie_max50_container)
    Lay_Right(politie_max50_container, politie_max50_component)
    Lay_Right(politie_max50_component, politie_max50_system_ext)
    Lay_Right(politie_max50_system_ext, politie_max50_container_ext)
    Lay_Right(politie_max50_container_ext, politie_max50_component_ext)
    Lay_Up(politie_max50_system, politie_min50_system)
}

Boundary(rs_limits, "rs_limits") {
    System(rs_limits_system, "rs_limits", "rs_limits", $sprite="rs_limits")
    System_Ext(rs_limits_system_ext, "rs_limits", "rs_limits", $sprite="rs_limits")
    Container(rs_limits_container, "rs_limits", "rs_limits", $sprite="rs_limits")
    Container_Ext(rs_limits_container_ext, "rs_limits", "rs_limits", $sprite="rs_limits")
    Component(rs_limits_component, "rs_limits", "rs_limits", $sprite="rs_limits")
    Component_Ext(rs_limits_component_ext, "rs_limits", "rs_limits", $sprite="rs_limits")
    Lay_Right(rs_limits_system, rs_limits_container)
    Lay_Right(rs_limits_container, rs_limits_component)
    Lay_Right(rs_limits_component, rs_limits_system_ext)
    Lay_Right(rs_limits_system_ext, rs_limits_container_ext)
    Lay_Right(rs_limits_container_ext, rs_limits_component_ext)
    Lay_Up(rs_limits_system, politie_max50_system)
}

Boundary(rs_user, "rs_user") {
    System(rs_user_system, "rs_user", "rs_user", $sprite="rs_user")
    System_Ext(rs_user_system_ext, "rs_user", "rs_user", $sprite="rs_user")
    Container(rs_user_container, "rs_user", "rs_user", $sprite="rs_user")
    Container_Ext(rs_user_container_ext, "rs_user", "rs_user", $sprite="rs_user")
    Component(rs_user_component, "rs_user", "rs_user", $sprite="rs_user")
    Component_Ext(rs_user_component_ext, "rs_user", "rs_user", $sprite="rs_user")
    Lay_Right(rs_user_system, rs_user_container)
    Lay_Right(rs_user_container, rs_user_component)
    Lay_Right(rs_user_component, rs_user_system_ext)
    Lay_Right(rs_user_system_ext, rs_user_container_ext)
    Lay_Right(rs_user_container_ext, rs_user_component_ext)
    Lay_Up(rs_user_system, rs_limits_system)
}

Boundary(internet, "internet") {
    System(internet_system, "internet", "internet", $sprite="internet")
    System_Ext(internet_system_ext, "internet", "internet", $sprite="internet")
    Container(internet_container, "internet", "internet", $sprite="internet")
    Container_Ext(internet_container_ext, "internet", "internet", $sprite="internet")
    Component(internet_component, "internet", "internet", $sprite="internet")
    Component_Ext(internet_component_ext, "internet", "internet", $sprite="internet")
    Lay_Right(internet_system, internet_container)
    Lay_Right(internet_container, internet_component)
    Lay_Right(internet_component, internet_system_ext)
    Lay_Right(internet_system_ext, internet_container_ext)
    Lay_Right(internet_container_ext, internet_component_ext)
    Lay_Up(internet_system, rs_user_system)
}

Boundary(github, "github") {
    System(github_system, "github", "github", $sprite="github")
    System_Ext(github_system_ext, "github", "github", $sprite="github")
    Container(github_container, "github", "github", $sprite="github")
    Container_Ext(github_container_ext, "github", "github", $sprite="github")
    Component(github_component, "github", "github", $sprite="github")
    Component_Ext(github_component_ext, "github", "github", $sprite="github")
    Lay_Right(github_system, github_container)
    Lay_Right(github_container, github_component)
    Lay_Right(github_component, github_system_ext)
    Lay_Right(github_system_ext, github_container_ext)
    Lay_Right(github_container_ext, github_component_ext)
    Lay_Up(github_system, internet_system)
}

Boundary(terraform, "terraform") {
    System(terraform_system, "terraform", "terraform", $sprite="terraform")
    System_Ext(terraform_system_ext, "terraform", "terraform", $sprite="terraform")
    Container(terraform_container, "terraform", "terraform", $sprite="terraform")
    Container_Ext(terraform_container_ext, "terraform", "terraform", $sprite="terraform")
    Component(terraform_component, "terraform", "terraform", $sprite="terraform")
    Component_Ext(terraform_component_ext, "terraform", "terraform", $sprite="terraform")
    Lay_Right(terraform_system, terraform_container)
    Lay_Right(terraform_container, terraform_component)
    Lay_Right(terraform_component, terraform_system_ext)
    Lay_Right(terraform_system_ext, terraform_container_ext)
    Lay_Right(terraform_container_ext, terraform_component_ext)
    Lay_Up(terraform_system, github_system)
}

Boundary(rs_pv, "rs_pv") {
    System(rs_pv_system, "rs_pv", "rs_pv", $sprite="rs_pv")
    System_Ext(rs_pv_system_ext, "rs_pv", "rs_pv", $sprite="rs_pv")
    Container(rs_pv_container, "rs_pv", "rs_pv", $sprite="rs_pv")
    Container_Ext(rs_pv_container_ext, "rs_pv", "rs_pv", $sprite="rs_pv")
    Component(rs_pv_component, "rs_pv", "rs_pv", $sprite="rs_pv")
    Component_Ext(rs_pv_component_ext, "rs_pv", "rs_pv", $sprite="rs_pv")
    Lay_Right(rs_pv_system, rs_pv_container)
    Lay_Right(rs_pv_container, rs_pv_component)
    Lay_Right(rs_pv_component, rs_pv_system_ext)
    Lay_Right(rs_pv_system_ext, rs_pv_container_ext)
    Lay_Right(rs_pv_container_ext, rs_pv_component_ext)
    Lay_Up(rs_pv_system, terraform_system)
}

Boundary(ic_node, "ic_node") {
    System(ic_node_system, "ic_node", "ic_node", $sprite="ic_node")
    System_Ext(ic_node_system_ext, "ic_node", "ic_node", $sprite="ic_node")
    Container(ic_node_container, "ic_node", "ic_node", $sprite="ic_node")
    Container_Ext(ic_node_container_ext, "ic_node", "ic_node", $sprite="ic_node")
    Component(ic_node_component, "ic_node", "ic_node", $sprite="ic_node")
    Component_Ext(ic_node_component_ext, "ic_node", "ic_node", $sprite="ic_node")
    Lay_Right(ic_node_system, ic_node_container)
    Lay_Right(ic_node_container, ic_node_component)
    Lay_Right(ic_node_component, ic_node_system_ext)
    Lay_Right(ic_node_system_ext, ic_node_container_ext)
    Lay_Right(ic_node_container_ext, ic_node_component_ext)
    Lay_Up(ic_node_system, rs_pv_system)
}

Boundary(rs_sts, "rs_sts") {
    System(rs_sts_system, "rs_sts", "rs_sts", $sprite="rs_sts")
    System_Ext(rs_sts_system_ext, "rs_sts", "rs_sts", $sprite="rs_sts")
    Container(rs_sts_container, "rs_sts", "rs_sts", $sprite="rs_sts")
    Container_Ext(rs_sts_container_ext, "rs_sts", "rs_sts", $sprite="rs_sts")
    Component(rs_sts_component, "rs_sts", "rs_sts", $sprite="rs_sts")
    Component_Ext(rs_sts_component_ext, "rs_sts", "rs_sts", $sprite="rs_sts")
    Lay_Right(rs_sts_system, rs_sts_container)
    Lay_Right(rs_sts_container, rs_sts_component)
    Lay_Right(rs_sts_component, rs_sts_system_ext)
    Lay_Right(rs_sts_system_ext, rs_sts_container_ext)
    Lay_Right(rs_sts_container_ext, rs_sts_component_ext)
    Lay_Up(rs_sts_system, ic_node_system)
}

Boundary(rs_role, "rs_role") {
    System(rs_role_system, "rs_role", "rs_role", $sprite="rs_role")
    System_Ext(rs_role_system_ext, "rs_role", "rs_role", $sprite="rs_role")
    Container(rs_role_container, "rs_role", "rs_role", $sprite="rs_role")
    Container_Ext(rs_role_container_ext, "rs_role", "rs_role", $sprite="rs_role")
    Component(rs_role_component, "rs_role", "rs_role", $sprite="rs_role")
    Component_Ext(rs_role_component_ext, "rs_role", "rs_role", $sprite="rs_role")
    Lay_Right(rs_role_system, rs_role_container)
    Lay_Right(rs_role_container, rs_role_component)
    Lay_Right(rs_role_component, rs_role_system_ext)
    Lay_Right(rs_role_system_ext, rs_role_container_ext)
    Lay_Right(rs_role_container_ext, rs_role_component_ext)
    Lay_Up(rs_role_system, rs_sts_system)
}

Boundary(kubernetes, "kubernetes") {
    System(kubernetes_system, "kubernetes", "kubernetes", $sprite="kubernetes")
    System_Ext(kubernetes_system_ext, "kubernetes", "kubernetes", $sprite="kubernetes")
    Container(kubernetes_container, "kubernetes", "kubernetes", $sprite="kubernetes")
    Container_Ext(kubernetes_container_ext, "kubernetes", "kubernetes", $sprite="kubernetes")
    Component(kubernetes_component, "kubernetes", "kubernetes", $sprite="kubernetes")
    Component_Ext(kubernetes_component_ext, "kubernetes", "kubernetes", $sprite="kubernetes")
    Lay_Right(kubernetes_system, kubernetes_container)
    Lay_Right(kubernetes_container, kubernetes_component)
    Lay_Right(kubernetes_component, kubernetes_system_ext)
    Lay_Right(kubernetes_system_ext, kubernetes_container_ext)
    Lay_Right(kubernetes_container_ext, kubernetes_component_ext)
    Lay_Up(kubernetes_system, rs_role_system)
}

Boundary(arcgis, "arcgis") {
    System(arcgis_system, "arcgis", "arcgis", $sprite="arcgis")
    System_Ext(arcgis_system_ext, "arcgis", "arcgis", $sprite="arcgis")
    Container(arcgis_container, "arcgis", "arcgis", $sprite="arcgis")
    Container_Ext(arcgis_container_ext, "arcgis", "arcgis", $sprite="arcgis")
    Component(arcgis_component, "arcgis", "arcgis", $sprite="arcgis")
    Component_Ext(arcgis_component_ext, "arcgis", "arcgis", $sprite="arcgis")
    Lay_Right(arcgis_system, arcgis_container)
    Lay_Right(arcgis_container, arcgis_component)
    Lay_Right(arcgis_component, arcgis_system_ext)
    Lay_Right(arcgis_system_ext, arcgis_container_ext)
    Lay_Right(arcgis_container_ext, arcgis_component_ext)
    Lay_Up(arcgis_system, kubernetes_system)
}

Boundary(rws, "rws") {
    System(rws_system, "rws", "rws", $sprite="rws")
    System_Ext(rws_system_ext, "rws", "rws", $sprite="rws")
    Container(rws_container, "rws", "rws", $sprite="rws")
    Container_Ext(rws_container_ext, "rws", "rws", $sprite="rws")
    Component(rws_component, "rws", "rws", $sprite="rws")
    Component_Ext(rws_component_ext, "rws", "rws", $sprite="rws")
    Lay_Right(rws_system, rws_container)
    Lay_Right(rws_container, rws_component)
    Lay_Right(rws_component, rws_system_ext)
    Lay_Right(rws_system_ext, rws_container_ext)
    Lay_Right(rws_container_ext, rws_component_ext)
    Lay_Up(rws_system, arcgis_system)
}

Boundary(rs_cm, "rs_cm") {
    System(rs_cm_system, "rs_cm", "rs_cm", $sprite="rs_cm")
    System_Ext(rs_cm_system_ext, "rs_cm", "rs_cm", $sprite="rs_cm")
    Container(rs_cm_container, "rs_cm", "rs_cm", $sprite="rs_cm")
    Container_Ext(rs_cm_container_ext, "rs_cm", "rs_cm", $sprite="rs_cm")
    Component(rs_cm_component, "rs_cm", "rs_cm", $sprite="rs_cm")
    Component_Ext(rs_cm_component_ext, "rs_cm", "rs_cm", $sprite="rs_cm")
    Lay_Right(rs_cm_system, rs_cm_container)
    Lay_Right(rs_cm_container, rs_cm_component)
    Lay_Right(rs_cm_component, rs_cm_system_ext)
    Lay_Right(rs_cm_system_ext, rs_cm_container_ext)
    Lay_Right(rs_cm_container_ext, rs_cm_component_ext)
    Lay_Up(rs_cm_system, rws_system)
}

'end-generated-plantuml

@enduml
```
